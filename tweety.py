#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
tweety.py: Continuously calculate the sliding avg of temps across last n tweets.

Uses the pub/nub and weather APIs to calculate sliding average of temperatures
across all locations of the last n tweets.
"""

import click

from client import Twitter
from client import Listener


@click.command()
@click.option("--message-count", default=0,
              help="Number of messages with location to process. 0 (default) => unlimited.")
@click.option("--window-size", default=5,
              help="Size of the sliding window (default: 5).",
              type=click.IntRange(2, 100))
@click.option('--debug', default=False,
              help="Write debug output to console (default: false).")
@click.option('--append', default=False,
              help="Open output files in append mode (default: false).")
@click.option('--temp-out', default="temp.out",
              help="Temperature output file name (default: temp.out).")
@click.option('--average-out', default="average.out",
              help="Average temp output file name (default: average.out).")
def main(message_count: int,
         window_size: int,
         debug: bool,
         append: bool,
         temp_out: str,
         average_out: str):
    """

    """
    # initialize a tweet handler
    if debug:
        print(f"message count: {message_count}, window size: {window_size},"
              f" debug: {debug}, append: {append}, temp_out: {temp_out},"
              f"average_out: {average_out}")
    handler: Listener = Listener(window_size=window_size,
                                 debug=debug,
                                 append=append,
                                 temp_out=temp_out,
                                 average_out=average_out)
    # start the control loop
    t: Twitter = Twitter(message_count=message_count, debug=debug)
    t.add_listener(handler)
    t.stream_tweets()
    # Done streaming, close the output files.
    handler.close()


# Press the green button in the gutter to run the script.
if __name__ == "__main__":
    main()
