# -*- coding: utf-8 -*-

"""
client.py: Client integration code.

API clients to interact with the weather and twitter APIs.
"""
import collections
import json
import requests
from collections import deque


from exceptions import TwitterError
from exceptions import TweetyError


class Listener:
    """
    Callback class to handle incoming tweet stream.
    """

    def __init__(self,
                 window_size: int,
                 debug: bool,
                 append: bool,
                 temp_out: str,
                 average_out: str):
        """
        Listener initializer.
        """

        self.weather: WeatherApiClient = WeatherApiClient(debug=debug)
        self.window: deque = collections.deque(maxlen=window_size)
        self.window_size: int = window_size
        self.debug: bool = debug

        # open files in append mode if specified
        mode = "a" if append else "w"
        try:
            self.temp_out = open(temp_out, mode)
        except FileNotFoundError:
            raise TweetyError(f"File not found {temp_out}. Please check file path.")
        try:
            self.average_out = open(average_out, mode)
        except FileNotFoundError:
            raise TweetyError(f"File not found {average_out}. Please check file path.")

    def message(self, tweet: dict):
        """
        Callback to handle tweet.
        """

        # these we want, user opted-in to share location
        if self.debug:
            print("========================================================")

        # let's grab the first location
        country = tweet["includes"]["places"][0]["country_code"]
        full_name = tweet["includes"]["places"][0]["full_name"]

        # grab the bounding box
        bbox = tweet["includes"]["places"][0]["geo"]["bbox"]

        # used for validation - https://www.gps-coordinates.net
        longs: [float] = [bbox[i] for i in range(len(bbox)) if i % 2 == 0]
        lats: [float] = [bbox[i] for i in range(len(bbox)) if i % 2 == 1]
        lat: float = sum(lats)/len(lats)
        long: float = sum(longs)/len(longs)

        # get the current temperature based on latitude and longitude.
        current_temp = self.weather.get_temperature(lat, long)

        # record the last message
        self.window.append(current_temp)
        average: float = sum(self.window)/len(self.window)
        # generate the output including the average of the last 5 message.

        # Write temperature to temp file
        self.temp_out.write(f"{current_temp:.2f}\n")
        # write out the average temperature to average.out
        self.average_out.write(f"{average:.2f}\n")

        if self.debug:
            print(f"Tweet location: {full_name}, country: {country}, temp_f: "
                  f"{current_temp}, avg of last {self.window_size}: {average:.2f}")

            # print(tweet)

    def close(self):
        """
        Cleanup, closes out open files.
        """
        self.temp_out.close()
        self.average_out.close()


class Twitter:
    """
    Twitter API client.

    Uses the sampled stream endpoint to retrieve 1% of tweets in real time.
    see https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/introduction
    """
    def __init__(self, message_count: int, debug: bool):
        """
        Config initializer for the twitter client.
        """
        self.bearer_token: str = "AAAAAAAAAAAAAAAAAAAAAOt8XgEAAAAAtnPmyV5MS%2FcZ5%2BjT%2BZg%2B3Ww59kY%3DOJO6YvoNV4QRcWYHW4xbqyQydcNuomQTfPJhvahMEm0bRrdUK9"
        self.base_url: str = "https://api.twitter.com/2/tweets/sample/stream"
        self.listeners: [Listener] = []
        self.message_count: int = message_count
        self.debug = debug

    def bearer_auth(self, r):
        """
        Method required by bearer token authentication.
        """

        r.headers["Authorization"] = f"Bearer {self.bearer_token}"
        r.headers["User-Agent"] = "tweety"
        return r

    def add_listener(self, listener: Listener):
        """
        Method to add a listener to the message stream.
        """
        self.listeners.append(listener)

    def stream_tweets(self):
        """
        Get the tweet stream.
        """
        # no point doing anything if listeners aren't defined
        if not self.listeners:
            if self.debug:
                print("Warning: No listeners defined!")
            # TODO: change to some kind of runtime exception(?)
            raise TwitterError("No listeners defined.")

        response = requests.get(
            self.base_url,
            auth=self.bearer_auth,
            stream=True,
            params={
                "expansions": "geo.place_id",
                "place.fields": "place_type,country_code,geo",
                "tweet.fields": "geo"
            }
        )

        if response.status_code == 200:
            if self.debug:
                print(f"{response.status_code}")

            count: int = 0
            for response_line in response.iter_lines():
                if response_line:
                    json_response = json.loads(response_line)
                    geo = json_response["data"]["geo"]

                    # These are not the tweets you're looking for
                    # user didn't opt for location.
                    if not geo:
                        continue
                    count += 1
                    for listener in self.listeners:
                        listener.message(json_response)
                    if self.debug:
                        print(f"count {count}")
                # if a message count is specified, break out on hitting it.
                if self.message_count and count >= self.message_count:
                    break
        else:
            raise TwitterError(f"Bad Twitter response: {response.status_code}.")

        return response


class WeatherApiClient:
    """
    Weather API client to get current temperature for given lat/long.
    """

    def __init__(self, debug):
        """Config initializer."""
        # TODO: Hard-coded config, should not be here for production usage.
        self.api_key = "a0254a63599446f8a1131311211511"
        self.base_url = "http://api.weatherapi.com/v1/current.json"
        self.debug = debug

    def get_temperature(self, lat: float, long: float):
        """
        Get temperature of specified location.

        :param lat: Latitude
        :param long: Longitude
        :return:
        """
        response = requests.get(self.base_url, params={"key": self.api_key, "q": f"{lat},{long}"})

        return response.json()["current"]["temp_f"]

