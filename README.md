## Tweety

Continuously calculate the sliding avg of the temperatures across all locations
of the last n tweets.

## Overview
The application should work on versions of python 3.8+. The following command line options are supported: 

```fish
(venv) nehar@mac2021 ~/s/tweety (main)> ./tweety.py --help
Usage: tweety.py [OPTIONS]



Options:
  --message-count INTEGER      Number of messages with location to process. 0
                               (default) => unlimited.
  --window-size INTEGER RANGE  Size of the sliding window (default: 5).
                               [2<=x<=100]
  --debug BOOLEAN              Write debug output to console (default: false).
  --append BOOLEAN             Open output files in append mode (default:
                               false).
  --temp-out TEXT              Temperature output file name (default:
                               temp.out).
  --average-out TEXT           Average temp output file name (default:
                               average.out).
  --help                       Show this message and exit.
(venv) nehar@mac2021 ~/s/tweety (main)>
```

## Files
```fish
(venv) nehar@mac2021 ~/s/f/tweety (main)> ls -l
total 208
-rw-r--r--  1 nehar  staff   5567 Jan 18 00:10 README.md
-rw-r--r--  1 nehar  staff   6637 Jan 18 00:10 client.py
-rw-r--r--  1 nehar  staff    640 Jan 18 00:10 exceptions.py
-rw-r--r--  1 nehar  staff     70 Jan 18 00:10 requirements.txt
-rw-r--r--  1 nehar  staff  73034 Jan 18 00:10 test_tweety.py
-rwxr-xr-x  1 nehar  staff   2093 Jan 18 00:10 tweety.py
-rw-r--r--@ 1 nehar  staff   1600 Jan 18 00:44 pubnub_example.py
(venv) nehar@mac2021 ~/s/f/tweety (main)>
```
* **README.md**
	* This file
* **client.py**
	* **Listener**: Message handler class.
	* **Twitter**: Twitter stream event loop.
	* **WeatherAPIClient**: Client for weatherapi.
* **exceptions.py**
	* 	**TwitterError**: Class demostrating twitter API specific error handling.
	*  **TweetyError**: Class demostrating application-specific error handling.
* **requirements.txt**
	* Application dependencies
* **test_tweety.py**
	* **mocked\_requests\_get**: Method demonstrating mocking requests API responses.
	* **application teset cases**
* **tweety.py**: Application entry point.
* **pubnub_example.py**: Sample code that uses the pubnub API.


## Getting Started

### Clone repository from gitlab
```fish
nehar@VONC02F43SHMD6M ~/src> git clone https://gitlab.com/nehararora/tweety.git
Cloning into 'tweety'...
remote: Enumerating objects: 71, done.
remote: Counting objects: 100% (71/71), done.
remote: Compressing objects: 100% (38/38), done.
remote: Total 71 (delta 35), reused 66 (delta 30), pack-reused 0
Receiving objects: 100% (71/71), 33.24 KiB | 8.31 MiB/s, done.
Resolving deltas: 100% (35/35), done.
nehar@VONC02F43SHMD6M ~/src> cd tweety/
nehar@VONC02F43SHMD6M ~/s/tweety (main)> ls
README.md        client.py        exceptions.py    requirements.txt test_key         test_tweety.py   tweety.py
nehar@VONC02F43SHMD6M ~/s/tweety (main)>
```

### Install Project virtualenv 

Tested with python 3.9 and 3.10. Note source file will be different 
across different shells.

```fish
nehar@VONC02F43SHMD6M ~/s/tweety (main)> python3 -m venv venv
nehar@VONC02F43SHMD6M ~/s/tweety (main)> ls
README.md        client.py        exceptions.py    requirements.txt test_key         test_tweety.py   tweety.py        venv

nehar@VONC02F43SHMD6M ~/s/tweety (main)> source venv/bin/activate.fish
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)> ls
README.md        client.py        exceptions.py    requirements.txt test_key         test_tweety.py   tweety.py        venv
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)>
```

### Install requirements
```fish
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)> pip install -r requirements.txt
Collecting requests==2.26.0
  Using cached requests-2.26.0-py2.py3-none-any.whl (62 kB)
        ...
        ...
        ...
Installing collected packages: pyparsing, tomli, toml, py, pluggy, packaging, iniconfig, coverage, attrs, urllib3, pytest, idna, charset-normalizer, certifi, requests, pytest-cov, click
Successfully installed attrs-21.4.0 certifi-2021.10.8 charset-normalizer-2.0.10 click-8.0.3 coverage-6.2 idna-3.3 iniconfig-1.1.1 packaging-21.3 pluggy-1.0.0 py-1.11.0 pyparsing-3.0.6 pytest-6.2.5 pytest-cov-3.0.0 requests-2.26.0 toml-0.10.2 tomli-2.0.0 urllib3-1.26.8
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)>
```

### Run Tests
```fish
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)> pytest --cov
==================================================================================================================================================== test session starts =====================================================================================================================================================
platform darwin -- Python 3.9.8, pytest-6.2.5, py-1.11.0, pluggy-1.0.0
rootdir: /Users/nehar/src/tweety
plugins: cov-3.0.0
collected 2 items

test_tweety.py ..                                                                                                                                                                                                                                                                                                      [100%]

---------- coverage: platform darwin, python 3.9.8-final-0 -----------
Name             Stmts   Miss  Cover
------------------------------------
client.py           88      1    99%
exceptions.py        9      0   100%
test_tweety.py      38      1    97%
------------------------------------
TOTAL              135      2    99%


===================================================================================================================================================== 2 passed in 14.96s =====================================================================================================================================================
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)>
```

### Run Application
```fish
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)> ./tweety.py --message-count 2 --window-size 2 --debug true
message count: 2, window size: 2, debug: True, append: False, temp_out: temp.out,average_out: average.out
200
========================================================
Tweet location: Makkah Al Mukarrama, Kingdom of Saudi Arabia, country: SA, temp_f: 62.2, avg of last 2: 62.20
count 1
========================================================
Tweet location: Sao Paulo, Brazil, country: BR, temp_f: 80.6, avg of last 2: 71.40
count 2
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)> cat temp.out
62.20
80.60
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)> cat average.out
62.20
71.40
(venv) nehar@VONC02F43SHMD6M ~/s/tweety (main)>
```