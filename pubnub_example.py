# -*- coding: utf-8 -*-
"""

"""

from pubnub.callbacks import SubscribeCallback
from pubnub.enums import PNStatusCategory
from pubnub.pnconfiguration import PNConfiguration
from pubnub.pubnub import PubNub
from pprint import pprint

client_id = "330066e4-344b-4832-92e2-823ee44e12d7"

pnconfig = PNConfiguration()
pnconfig.subscribe_key = "sub-c-cd095dbe-4516-11ec-8ee6-ce954cd2c970"
# pnconfig.subscribe_key = "sub-c-78806dd4-42a6-11e4-aed8-02ee2ddab7fe"  # pubnub sub key
# pnconfig.publish_key = "pub-c-db59e68f-30af-41b6-a037-2a8a203f8b4f"
pnconfig.uuid = "330066e4-344b-4832-92e2-823ee44e12d7"
pubnub = PubNub(pnconfig)


class MySubscribeCallback(SubscribeCallback):

    def presence(self, pubnub, presence):
        pass

    def status(self, pubnub, status):
        messagess = pubnub.fetch_messages()
        print("========== Status called ==========")
        print(dir(status))
        print(f"Status({status.status_code})")
        print(f"Status({status.client_response})")
        print("========== Status end ==========")

    def message(self, pubnub, message):
        """
        Callback to handle new message coming in on the channel.

        :param pubnub:
        :param message:
        :return:
        """
        print("========== Message called ==========")
        print(f"{message}")
        print("========== Message end ==========")


pubnub.add_listener(MySubscribeCallback())

pubnub.subscribe().channels(["pubnub-twitter"]).execute()

