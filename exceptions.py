# -*- coding: utf-8 -*-
"""
exceptions.py; Application specific exceptions.
"""


class TwitterError(Exception):
    """
    Twitter client errors.
    """
    def __init__(self, message: str):
        """
        Exception initializer.
        :param message: Error message returned.
        """
        super().__init__(message)
        self.message = message


class TweetyError(Exception):
    """
    Application errors.
    """
    def __init__(self, message: str):
        """
        Exception initializer.
        :param message: Error message returned.
        """
        super().__init__(message)
        self.message = message
